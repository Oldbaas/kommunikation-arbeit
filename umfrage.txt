Guten Tag

Wir möchten im Rahmen einer Projektarbeit an der Berner Fachhochschule herausfinden ob Informatiker in der Schweiz eher intro- oder extravertiert sind.
Dafür möchten wir mithilfe einer Umfrage Daten zum Thema sammeln.

Es gibt zwei Versionen der Umfrage, eine mit Fragen welche spezifisch für die Informatikbranche zugeschnitten sind, sowie eine für die Kontrollgruppe zum vergleich mit nicht-Informatikern.
Das Ausfüllen der Umfrage dauert ca. 5 Minuten.

Wenn Sie in der Informatikbranche tätig sind, füllen Sie bitte diese Umfrage aus:
  https://goo.gl/forms/os6lwE6wtnPbvcjv2

Wenn Sie in einer anderen Branche tätig sind, füllen Sie bitte diese Umfrage aus:
  https://goo.gl/forms/GRp2JtBvlmzqGygw2

Damit die Umfrage repräsentativer wird, bitten wir Sie diese Nachricht an Ihre Kontakte weiterzuleiten.

Vielen Dank für Ihre Teilnahme.

Freundliche Grüsse

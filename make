#!/bin/bash
pandoc --filter pandoc-citeproc --csl=custom.csl --number-sections --listings --template ./.latex/eisvogel.tex --pdf-engine=xelatex ./Arbeit.md -o ./Arbeit.pdf

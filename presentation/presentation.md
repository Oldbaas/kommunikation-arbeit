---
title: "Intro- und Extraversion in der Informatikbranche"
author:
  - Kaderli Severin
  - Schär Marius
  - Ratnasabapathy Vipushan
  - Zhegrova Ardit
date:         "2018-12-21"
ort:          "Bern"
titlepage:    true
lang:         de-CH
toc:          true
toc-own-page: true
toc-title:    "Agenda"
links-as-notes:
  - true
bibliography: citations.bib
fontsize: 12pt
classoption:
  - aspectratio=169
header-includes: |
    \usepackage{fancyhdr}
    \usepackage{xcolor}
    \usetheme{metropolis}
    \usepackage{scrextend}
    \changefontsizes{14pt}
    \usepackage[font=scriptsize,labelfont=bf]{caption}
    \newcommand{\colsbegin}{\begin{columns}}
    \newcommand{\colsend}{\end{columns}}
---

## Fragestellung
> "Sind Informatiker wirklich Introvertierter als andere Berufsgruppen?"

# Einführung

## Ausgangslage
- Vorurteile
- Studie von Gnambs in 2015 ($N = 1695$).

## Projektablauf
1) Persönlichkeitstests studieren
2) Umfrage designen
3) Umfrage auswerten

## Methoden
- Kontrollgruppe
- Umfrage
  * Persönlichkeitstest
  * Demographische Daten
  * Arbeitsumfeld

## Introversion / Extraversion
> "Introversion und Extraversion sind zwei Pole einer \
> Persönlichkeitseigenschaft, die durch die
> Interaktion \
mit der Umwelt charakterisiert wird."

# Umfragen
## Umfragen
- Persönlichkeitstest
- demographische Einordnung
- Arbeitsumfeld

## Persönlichkeitstest
\colsbegin
\column{.3\textwidth}
\begin{figure}
  \centering
  \includegraphics[width=2.5cm] {assets/mccroskey.jpg}
  \caption{Dr. James McCroskey \dagger}
\end{figure}
\column{.7\textwidth}

- nach McCroskey, basierend auf Eysenck
- teilt Persönlichkeit auf in drei Dimensionen
- **Extraversion**, Neurotizismus und Psychotizismus

\colsend

## Persönlichkeitstest
Teilnehmer bewerten 17 Fragen auf einer Skala von 1 - 5
 ^(trifft\ zu\ /\ triff\ nicht\ zu)^ :

 1. ) Halten Sie sich bei gesellschaftlichen Anlässen oft im Hintergrund?
 2. ) Treffen Sie sich gerne mit anderen Leuten?
 3. ) \textcolor{black!40}{Variiert Ihr Gemüt of ohne ersichtlichen Grund?}
 4. ) \textcolor{black!40}{Sind Sie oft launisch?}
 5. ) Integrieren Sie sich schnell in eine neue Gruppe?
 6. ) Fühlen Sie sich sehr wohl an gesellschaftlichen Anlässen?
 7. ) Erlangen Sie Zufriedenheit durch sozialisieren mit Anderen?
 8. ) $\dots$

## Indexbewertung
$$
index = 12 - \sum Frage_{1,4} + \sum Frage_{2,5,7,8,10,11,13,14,15,17}
$$

. . .

$$
12 \leq index \leq 60
$$

. . .

$$
\text{Ergebnis} =
\begin{cases}
  \text{introvertiert} & \quad \text{falls}\;index \geq 40 \\
  \text{extravertiert} & \quad \text{falls}\;index \leq 32 \\
  \text{ambivertiert}  & \quad \text{falls}\; 32 < index < 40
\end{cases}
$$

## Andere Daten
- Alter
- Geschlecht
- Wohnort
- Arbeitsplatz
  * Teamgrösse
  * Firmengrösse
  * Position
- Ausbildung

# Resultate
## Resultate
* Informatikbranche
  * 30 Resultate
* Kontrollgruppe
  * 42 Resultate

## Geschlechtsverteilung
\colsbegin

\column{.5\textwidth}

![Geschlecht (Informatiker)\label{gender}](../assets/geschlecht.png)

\column{.5\textwidth}

![Geschlecht (Kontrollgruppe)\label{gender_control}](../assets/geschlecht_kontrollgruppe.png)

\colsend

## Altersverteilung
\colsbegin

\column{.5\textwidth}

![Altersverteilung (Informatiker)\label{age}](../assets/alter.png)

\column{.5\textwidth}

![Altersverteilung (Kontrollgruppe)\label{age_control}](../assets/alter_kontrollgruppe.png)

\colsend

## Introversion (Informatikgruppe)
\colsbegin

\column{.5\textwidth}

![Introversion (Informatikbranche) \label{intro_1}](../assets/intro_1.png)

\column{.5\textwidth}

![Introversion - Detailliert (Informatikbranche) \label{intro_2}](../assets/intro_2.png)

\colsend

## Selbsteinschätzung vs. Test (Informatikgruppe)
\colsbegin

\column{.5\textwidth}

![Vergleich zwischen Index und Selbsteinschätzung (Informatikbranche) \label{intro_it_idx_v_s}](../assets/intro_it_idx_vs_self.png)

\column{.5\textwidth}

![Selbsteinschätzung vs. Testresultat (Informatikbranche) \label{self_v_idx_it}](../assets/intro_idx_vs_self_trend.png)

\colsend

## Introversion (Kontrollgruppe)
\colsbegin

\column{.5\textwidth}

![Introversion (Kontrollgruppe) \label{intro_3}](../assets/intro_3.png)

\column{.5\textwidth}

![Introversion - Detailliert (Kontrollgruppe) \label{intro_4}](../assets/intro_4.png)

\colsend

## Selbsteinschätzung vs. Test (Kontrollgruppe)
\colsbegin

\column{.5\textwidth}

![Vergleich zwischen Index und Selbsteinschätzung (Kontrollgruppe) \label{intro_control_idx_v_s}](../assets/intro_control_idx_vs_self.png)

\column{.5\textwidth}

![Selbsteinschätzung vs. Testresultat (Kontrollgruppe) \label{control_self_v_idx_it}](../assets/control_intro_idx_vs_self_trend.png)


\colsend

# Ergebnisse

## Zusammenfassung

Informatiker sind Extravertiert

## Schlussfolgerung
- Unterricht


## Diskussion
- 30 Informatiker
- Verschiedene Resultate

## Ausblick
- McKroskey-Test
- Mehr Teilnehmer
- Mehr Informationen

---
title: "Intro- und Extraversion in der Informatikbranche"
author:
  - Kaderli Severin
  - Schär Marius
  - Ratnasabapathy Vipushan
  - Zhegrova Ardit
date: "2018-12-21"
subtitle:	"Semesterarbeit Kommunikation 2"
submission: true
submission-location: "Bern"
extra-info: true
institute: "Berner Fachhochschule"
department:	"Technik und Informatik"
lecturer:	"Thomas von Burg"
lof: true
lang: "de-CH"
links-as-notes:
  - true
geometry:
  - margin=2.5cm
bibliography: citations.bib
abstract: |  
  Das Vorurteil, dass Informatiker introvertiert sind, ist weit verbreitet,
  auch unter Informatikern. Stimmt dieses Vorurteil, oder ist es komplett
  fabriziert? In einer früheren Studie hat man herausgefunden, dass
  Informatiker tendenziell introvertiert sind ^[@gnambs2015]. Wir wollten
  diese Resultate mit dieser Arbeit selber reproduzieren.

  Dazu haben wir eine Gruppe von
  Informatikern ($N_{\text{Informatik}} = 30$) sowie eine
  Kontrollgruppe ($N_{\text{Kontrolle}} = 42, N_{\text{Total}} = 72$)
  aus anderen Berufsfeldern in einer Online-Umfrage
  befragt und die Resultate ausgewertet. Der Introversionsgrad wurde
  mittels eines Persönlichkeitstests nach James McCroskey [@mccroskey03]
  bestimmt.

  Unsere Studie zeigt dass 40 Prozent der Informatiker eher introvertiert sind.
  Dies entspricht in etwa dem Verhältnis welches vorhergehende Studien für
  die Gesamtbevölkerung finden [@cain2012quiet]. Unser Vergleich zwischen dem
  Persönlichkeitstest und der Selbsteinschätzung zeigt auch dass sich
  Informatiker als eher Introvertierter einschätzen als der Test.

  Unsere Kontrollgruppe ist zu 65 Prozent eher introvertiert. Dies ist
  überraschend und veranslasst uns dazu den Persönlichkeitstest nach
  McCroskey skeptisch zu betrachten. Die Personen der Kontrollgruppe
  schätzen sich selbst eher extravertierter ein als der Test.

  In allem zeigt diese Studie vor allem dass für Untersuchungen dieser Art
  eine grosse Stichprobe \
  voraussetzt um gute Datenqualität zu erreichen,
  was in unserem Rahmen leider nicht
  möglich war.
---

# Einleitung
Informatiker sind introvertiert, so lautet zumindest das verbreitete Vorurteil. 
Die Absicht dieser Arbeit ist es zu Untersuchen, ob dieses Vorurteil mit der Realität 
übereinstimmt, oder ob es keinen Unterschied in der Introvertiertheit von Informatikern gegenüber 
anderen Berufsgruppen gibt. 
Zusätzlich möchten wir die befragten Informatiker noch abgrenzen. Mit der 
Abgrenzung überprüfen  wir ob zum Beispiel das Alter, der Wohnort, die 
Ausbildungsstufe oder ob es eine Rolle spielt in welcher Abteilung als Informatiker
man arbeitet und ob Leute in einer Kader/ Teamleiter Position die Introversion oder 
Extraversion beeinflussen. 
Mit den spezifischen Fragen, die wir den Informatikern stellen dient dazu um zu 
überprüfen ob die Befragten eher introvertiert oder extravertier sind. Den Kontrollgruppen 
werden die gleichen Fragen gestellt um die Analyse der beiden Gruppen zu vereinfachen.

## Ausgangslage
Es gibt etliche Studien die besagen, dass Informatiker generell introvertiert sind. Einige behaupten
sogar das Informatiker die introvertierteste Berufsgruppe ist. Mit Hilfe unserer Arbeit haben wir die 
Absicht zu üperprüfen ob dieses Voruteil mit unseren Ergebnissen übereinstimmt.

## Was ist Introversion
Introversion und Extraversion sind zwei Pole einer Persönlichkeitseigenschaft, die durch die 
Interaktion mit der Umwelt charakterisiert wird. Das heisst: Eher extravertierte Menschen 
empfinden den Austausch und das Handeln in sozialen Gruppen als anregend, während eher introvertierte 
Menschen oft ihre Aufmerksamkeit und Energie auf ihr Innenleben fokussieren.
Extraversion ist eine Persönlichkeitsdimension von mehreren bekannten Persönlichkeitstheorien 
(Beispiel: Myers-Briggs-Typenindikator, Big Five, und Eysenck-Persönlichkeitsinventar.


## Zweck
Unsere Absicht mit dieser Arbeit ist es herauszufinden, ob das oben benannte Vorurteil 
gerechtfertigt ist. Da wir im Bereich Information und Technik arbeiten, haben wir eigene Intressen 
herauszufinden ob das Vorurteil in der Realität zutrifft.
Zudem möchten wir herausfinden, welche Unterschiede es zwischen Informatikern und 
anderen Berufsgruppen gibt und wir überprüfen ob sich die Befragten auch selbst richtig einschätzen können.


## Abgrenzung
Es wird nur die Informatikspezifisch angeschaut, die Analysen der anderen Berufsgruppen dienen nur zu 
Kontrollzwecken. Zudem möchten wir nur Korrelationen finden, nicht Kausalität beweisen.

\newpage
## Stand der Wissenschaft
In einem Artikel von 2016 sagt Leslie A. Gordon,
gestützt auf Susan Cain [@cain2012quiet],
dass eine signifikante Mehrheit von Rechtsanwälten (ca. $60\%$, bei Anwälten
für geistiges Eigentum gar $90\%$) introvertiert seien [@gordon2016].
Basierend darauf kann also davon ausgegangen werden, dass unterschiedliche
Berufsgruppen unterschiedliche Anteile an intro- beziehungsweise
extravertieren Arbeitern aufweisen.

Timo Gnambs hat im *Journal of Research and Personality* eine Meta-Analyse
publiziert, welche 19 Studien verglichen hat (total $N = 1695$), um
herauszufinden wie die Persönlichkeitsausprägungen aus dem Big Five Modell mit
der Fähigkeit eines Programmiers korrelieren. Er kam zum Schluss das
Programmierer generell eher leicht introvertiert sind [@gnambs2015].
Neben Introversion korrelieren auch Gewissenhaftigkeit und Offenheit
mit der Fähigkeit einer Person als Programmierer.

Hier ist zu beachten, dass Extraversion nur einen Korrelationskoeffizienten
von $\rho = -0.11$ aufweist, während Faktoren wie die generellen kognitiven
Fähigkeiten Werte von $\rho = 0.29$ zeigen und daher eine signifikantere
Korrelation darstellen.

## Projektablauf
Zu Beginn möchten wir der exakte Stand der Wissenschaft herausfinden. Mit den gewonnen Informationen 
möchten wir, mit Hilfe von Literaturen zu unserem Thema weiter festigen. Die erlangten Informationen 
werden uns dann, bei der Herstellung von Umfragen und später bei der Analyse von unseren Ergebnissen helfen.
Nach der Recherche müssen Daten gesammelt werden. Dazu verwenden wir die Online Umfrage. Diese Methode 
wird jeweils mit Gruppen von Informatikern und Kontrollgruppen durchgeführt. Nach einem gewissen Zeitraum
beenden wir die Online-Umfrage und beginnen mit der Analyse. Am Schluss halten wir alle unsere Ergebnisse 
und Analysen schriftlich fest.

# Verfahren und Methoden
Im Rahmen dieser Arbeit wollen wir herausfinden, ob das verbreitete Vorurteil,
dass Informatiker introvertiert sind stimmt und wie sich die Introversion
zwischen Informatikern und der generellen Bevölkerung unterscheidet, oder ob es
überhaupt gar keinen Unterschied gibt.

Um das Vorurteil zu überprüfen möchten wir durch eine Umfrage Datensammeln um 
nachhinein das
Introversionsgrad von Informatikern und von einer Kontrollgruppe mit Personen 
aus anderen Berufsgattungen zu bestimmen.

## Kontrollgruppen
Wir haben uns dazu entschieden die Umfrage mit Informatikern, als auch mit einer Kontrollgruppe 
von Nicht-Informatikern durchzuführen um festzustellen ob:

1. Informatiker eher introvertiert oder extravertiert sind.
2. Es einen Unterschied zwischen Informatikern und der generellen Bevölkerung
gibt.

## Umfrage
In unserer Umfrage sammeln wir demographische Daten, sowie Daten zur Tätigkeit
als Informatiker und führen dann einen Introversionstest nach James McCroskey
[@mccroskey03] durch.

In der Kontrollgruppe sammeln wir weniger Daten über die berufliche Tätigkeit,
die übrigen Fragen sind jedoch die selben. Uns interessiert bei der
Kontrollgruppe im beruflichen Aspekt nur die Branche in welcher die Teilnehmer
tätig sind.

# Hauptteil

## Introversion und Extraversion


Introversion und Extraversion sind angeborene Veranlagungen, auf die allerdings das soziale
Umfeld, die Erziehung sowie extreme Erlebnisse weiter Einfluss nehmen können. 
Jeder Mensch hat sowohl introvertierte als auch extravertierte Eigenschaften zusätzlich existiert 
auch ein Mittelpunkt: ambivertiert. Beide Typen reagieren unterschiedlich auf Impulse von aussen.
Die Eigenschaften von Extrovertierte und Introvertierte sind sehr unterschiedlich. Extrovertierte
sind kontaktfreudig, gesprächig, aktiv, sowie mimisch und gestisch ausdrucksstark. 
Die meisten Modelle verstehen darunter auch einen starken Willen zum Handeln und Gestalten.
Extrovertierte treten danach bestimmt und energisch auf.

Die Introvertierte sind hingegen zurückhaltend, schweigsam, ruhig, passiv, schüchtern. 
Sie ziehen sich gerne zurück, hören lieber zu und vermitteln mimisch und gestisch das 
Bild des stillen, konzentrierten Denkers. Während der Extrovertierte offen wirkt, wirken
sie eher verängstigt. 
Introvertierte Menschen sind in der Regel gute Zuhörer und denken Ideen zu ende, bevor 
sie diese aussprechen. Eine der großen Stärken der Introvertierten ist ihre Kreativität. 
Hinzu kommt noch, dass sie über mehr Einfühlungsvermögen verfügen als die Extrovertierten.
Introvertierte sind weniger mit ihrer Darstellung nach Außen beschäftigt, sondern 
konzentrieren sich eher auf das Wesentliche.

Aber es gibt auch viele, die ziemlich ausgeglichen zwischen den beiden Eigenschaften liegen. 
Die sogenannten Ambivert. Menschen mit ausgeglichenen Eigenschaften zeigen sowohl 
extrovertierte als auch introvertierte Reaktionen. 
So haben sie im Allgemeinen Spass daran unter Menschen zu sein, aber brauchen danach 
wieder Ruhe und geniessen die Einsamkeit.

\newpage
## Umfrage - Design
Die unserer Analyse zugrunde liegenden Daten stammen aus einer Online-Umfrage,
deren Rohdaten im Anhang zu finden sind.
Dieses Kapitel beschreibt die Umfrage im Detail.

### Selbsteinschätzung
Ganz zu Beginn der Umfrage erklären wir in Kurzform was
Introversion bzw. Extraversion ist:

> Introversion und Extraversion sind zwei Pole einer Persönlichkeitseigenschaft,
> die durch die
> Interaktion mit der Umwelt charakterisiert wird. Das heisst:
> Eher extravertierte Menschen
> empfinden den Austausch und das Handeln in sozialen Gruppen als anregend,
> während eher
> introvertierte Menschen oft ihre Aufmerksamkeit
> und Energie auf ihr Innenleben fokusieren

\noindent Nach dieser Kurzerklärung bitten wir um eine Selbsteinschätzung
auf einer Skala von 1 bis 10,
Introvertiert bis Extravertiert.
Wir vergleichen diese Einschätzung später mit dem Persönlichkeitstest 
nach McCroskey [@mccroskey03].

### Demographische Daten
Um korrelationen in der Introvertiertheit zwischen Informatikern und
anderen Berufsgruppen zu finden, brauchen wir demographische Daten
welche die Teilnehmer in Gruppen aufteilt.

Dazu fragen wir nach Geschlecht und Altersgruppe (intervalle von 10 Jahren),
sowie wo der Teilnehmer aufgewachsen ist
*[Stadt, Vorstadt, oder ländliche Gegend]* und wo der Teilnehmer jetzt wohnt.

Wir fragen auch ob die Teilnehmer beruflich tätig sind,
oder ob sie eine Ausbildung absolvieren. 

#### Am Arbeitsplatz
Hier erruiren wir ob der Teilnehmer in einer Grossfirma oder einem KMU
arbeiten \
(*"Wie viele Mitarbeiter hat das Unternehmen in dem Sie tätig sind?"*),
sowie ob der Teilnehmer in einer Führungsposition tätig ist.

Hier unterscheiden sich die Umfragen für Informatiker und
Kontrollgruppenteilnehmer: In der Kontrollgruppe, fragen wir nach der Branche
in der sie tätig sind, bei der Informatikergruppe wie gross das Team ist
in dem sie täglich arbeiten, sowie welche Rolle in der Informatikbranche sie
ausführen *[Software Entwicklung, Testing, Support, etc.]*. 

\newpage
#### Ausbildung
Auch hier sind die Umfragen der Informatiker und der Kontrollgruppe
unterschiedlich. In der Kontrollgruppe erfragen wir nur den Typ der
Ausbildung *[höhere Ausbildung, Gymnasium, Volksschule, Lehre]*,
während wir bei der Informatikergruppe nach einer Fachrichtung
in einem Studiengang fragen.

\newpage
### Extraversionstest
Der Extraversionstest ist in verkürzter Form von
Dr. James McCroskey übernommen. Dr. McCroskey wiederum, zieht diesen Test
aus früheren Untersuchungen durch Hans Eysenck[@mccroskey03], welcher in seinen
Untersuchungen die Persönlichkeit in drei Dimensionen aufteilt [@eysenck1975manual]:

- **E** - Extraversion/Introversion
- **N** - Neurotizismus/Stabilität
- **P** - Psychotizismus/Sozialisierung

Für diese Untersuchung ist nur **E** relevant. Nach Eysenck brauchen extravertierte
Menschen Stimulation von aussen, um ihr optimales
Erregungsniveau ^[***optimales Erregnungsniveau:*** der Zustand in welchem sich
eine Person am wohlsten fühlt] zu erreichen, während introvertierte Menschen
eher Überstimuliert sind, und sich daher öfter zurückziehen
und Zeit alleine Verbringen müssen um Ihr optimales Erregungsniveau zu erreichen.

Eysenck hat in seinem *Eysenck Personality Questionnaire*[@eysenck1975manual]
Fragen erfasst um den Persönlichkeitstypen zu bestimmen.
Diese Hypothesen hat er mit Messungen des Erregungsniveaus anhand der
elektrodermalen Aktivität ^[***elektrodermale Aktivität:***
Veränderung der elektrischen Eigenschaften der Haut],
Elektroenzephalografie ^[***Elektroenzephalografie:*** EEG, Gehirnaktivität], sowie
Schweissaktivität verglichen.

\newpage
Die Umfrageteilnehmer wählen bei jeder Frage einen Wert zwischen 1 - 5 \
(*"Trifft überhaupt nicht zu" - "Trifft sehr zu"*) aus. 
Zum Test gehören folgende Fragen:

1. ) Halten Sie sich bei gesellschaftlichen Anlässen oft im Hintergrund?
2. ) Treffen Sie sich gerne mit anderen Leuten?
3. ) Fühlen sie sich manchmal glücklich, manchmal deprimiert,
ohne offensichtlichen Grund?
4. ) Haben sie einen begrenzten Freundeskreis?
5. ) Treffen Sie sich oft mit anderen Leuten?
6. ) Variiert Ihr Gemüt of ohne ersichtlichen Grund?
7. ) Gehen Sie unbekümmert durchs Leben?
8. ) Können Sie an Partys loslassen und sich vergnügen?
9. ) Sind Sie oft launisch?
10. ) Wären Sie sehr unglücklich wenn Sie sich weniger oft
mit Anderen treffen könnten?
11. ) Ergreiffen Sie die Initiative wenn Sie jemand neues kennenlernen?
12. ) Streifen Sie oft ab, wenn Sie sich konzentrieren sollten?
13. ) Spielen Sie ihren Freunden gerne Streiche?
14. ) Integrieren Sie sich schnell in eine neue Gruppe?
15. ) Fühlen Sie sich sehr wohl an gesellschaftlichen Anlässen?
16. ) Verlieren Sie sich oft in Gedanken, auch wenn Sie an
einem Gespräch teilnehmen sollten?
17. ) Erlangen Sie Zufriedenheit durch sozialisieren mit Anderen?

Nicht alle diese Fragen haben direkt etwas mit Intro- oder Extraversion zu tun,
diese sind im Abschnitt (\ref{berechnung-index}) beschrieben.
Sie werden verwendet um Neurotizismus, die emotionale Labilität, zu messen.
Sie sind enthalten damit nicht nur Fragen welche direkt mit dem Messwert
in Verbindung stehen beantwortet werden.

\newpage
### Berechnung des Introversionsindexes \label{berechnung-index}
Die Fragen 3, 6, 9, 12, 15, und 16 werden **nicht** im Index eingerechnet,
da Sie nicht direkt mit Intro-/Extraversion zusammenhängen.
Zur berechnung des Index, wird also folgende Formel verwendet:
$$
  index = 12 - \sum Frage_{1,4} +
  \sum Frage_{2,5,7,8,10,11,13,14,15,17}
$$

Somit sollte $12 \leq index \leq 60$ gelten, wenn der $index$ korrekt
berechnet wurde [@mccroskey03]. Dieser $index$ ist wie folgt zu werten:
$$
\text{Ergebnis} =
\begin{cases}
  \text{introvertiert} & \quad \text{falls}\;index \geq 40 \\
  \text{extravertiert} & \quad \text{falls}\;index \leq 32 \\
  \text{ambivertiert}  & \quad \text{falls}\; 32 < index < 40
\end{cases}
$$

Der Begriff *"Ambivertiert"* wird für Individuen verwendet
die nicht auf die extremen des Spektrums fallen,
sondern sich in der Mitte befinden.

Für einen Vergleich mit anderen Studien, welche ihre Resultate nur in die
Kategorien Extra- und Introversion unterteilen, berechnen wir auch dies:
$$
\text{Ergebnis} =
\begin{cases}
  \text{introvertiert} & \quad \text{falls}\;index > 36 \\
  \text{extravertiert} & \quad \text{falls}\;index \leq 36 \\
\end{cases}
$$

Es ist zu erwarten das ungefähr 33-50% der Kontrollgruppe als
introvertiert klassifiziert werden [@cain2012quiet].

\newpage
## Umfrage - Resultate
Bei der Umfrage für die Informatikbranche haben wir 30 Antworten bekommen und bei der Umfrage für die Kontrollgruppe haben wir 42 Antworten erhalten.

### Geschlechtsverteilung
Das Geschlecht bei den Informatikern ist vorüberwiegend Männlich, was eigentlich
keine Überraschung darstellt. Da Frauen immer noch sehr wenig in der
Informatikbranche vertreten sind (siehe Abbildung \ref{gender}) [@bfs_gender_informatik].

Bei der Kontrollgruppe ist die Verteilung des Geschlechtes ausgeglichener
(siehe Abbildung \ref{gender_control}).
Diese Verteilung entspricht in etwa der schweizerweiten
Verteilung der Erwerbstätigen Personen [@bfs_gender].
Es kann also davon ausgegangen werden, dass unsere Umfrage was
Geschlechtsverteilung angeht, repräsentativ ist.

\colbegin[2]

![Geschlecht (Informatiker)\label{gender}](./assets/geschlecht.png)
 
\columnbreak

![Geschlecht (Kontrollgruppe)\label{gender_control}](./assets/geschlecht_kontrollgruppe.png)

\colend

\newpage
### Altersverteilung
In Abbildung \ref{age} und Abbildung \ref{age_control} kann man die jeweilige Altersverteilung bei der Informatikbranche bzw. bei der Kontrollgruppe sehen. Bei der Informatikbranche ist der grösste Teil im Bereich 20-30 Jahre anzusiedeln. Dies liegt höchstwahrscheinlich daran, dass wir unsere Kollegen aus der Informatikbranche zur Umfrage eingeladen haben und diese im gleichen Alter wie wir sind.

Bei der Kontrollgruppe hingegen sind die Altersgruppen 15-20 Jahre und 40-50 Jahre am meisten vertreten. Dies liegt unter anderem daran, dass wir eingie Gymnasialschüler gefragt haben, welches die Gruppe 15-20 Jahre erklärt und auf der anderen Seite unsere Elteren und andere Verwandten, welche sich eher in der Altergruppe 40-50 befinden.

\colbegin[2]

![Altersverteilung (Informatiker)\label{age}](./assets/alter.png)

\columnbreak

![Altersverteilung (Kontrollgruppe)\label{age_control}](./assets/alter_kontrollgruppe.png)

\colend

\newpage
### Introversion (Informatikgruppe)
Bei der Berechnung der Introversionsindexes bei der Informatikbranche sind wir auf
folgendes Resultat gestossen (siehe Abbildung \ref{intro_1}).

Wie man sehen kann ist ein grosser Teil der Informatiker als extravertiert eingeordnet geworden.
Dies widerspricht eigentlich der gängigen Meinung,
dass ein grosser Teil der Informatiker introvertiert sind.

Wenn man nun die detailierte Darstellung der Introversion ansieht
(siehe Abbildung \ref{intro_2}) sieht man, dass ein grosser Teil
der Introvertierten eher in der Mitte als ambivertiert einzuordnen sind.

\colbegin[2]

![Introversion (Informatikbranche) \label{intro_1}](./assets/intro_1.png)

\columnbreak

![Introversion - Detailliert (Informatikbranche) \label{intro_2}](./assets/intro_2.png)

\colend

\newpage
### Selbsteinschätzung vs. Test (Informatikgruppe)
In Abbildung \ref{intro_it_idx_v_s} sehen wir den Vergleich zwischen dem
Introversionsindex und der Selbsteinschätzung. Es zu sehen dass sich
mehr als introvertiert einschätzen, als der Test bestätigt.
In Abbildung \ref{self_v_idx_it} ist Abgebildet wie sehr sich die
Selbsteinschätzung vom Testresultat unterscheidet.
Ein negativer Wert bedeutet, dass sich der Teilnehmer als introvertierter
als der Test einschätzt, ein positiver Wert dass sich der Teilnehmer als
extravertierter einschätzt.

Insgesamt zeigt uns die rote Trendlinie in Abbildung \ref{self_v_idx_it},
dass sich unsere Teilnehmer als leicht introvertierter einschätzen
als sie vom Test klassifiziert werden. Der Trend und unsere Samplesize sind
aber so klein, dass hier keine statistische Signifikanz vorliegt.

\colbegin[2]

![Vergleich zwischen Index und Selbsteinschätzung (Informatikbranche) \label{intro_it_idx_v_s}](./assets/intro_it_idx_vs_self.png)

\columnbreak

![Selbsteinschätzung vs. Testresultat (Informatikbranche) \label{self_v_idx_it}](./assets/intro_idx_vs_self_trend.png)

\colend

\newpage
### Introversion (Kontrollgruppe)
Bei der Kontrollgruppe sind wir auf ein unerwartetes Resultat gestossen (siehe Abbildung \ref{intro_3}).
Hier ist ein Grossteil der Befragten ($66.7\%$) als introvertiert eingestuft geworden.
Dies ist zum einen etwas mehr als in der Informatikgruppe, andererseits auch um einiges mehr als
in anderen Studien ermittelt wurde.
Hier ist die Genauigkeit des McCroskey-Tests zu hinterfragen, den in vorherigen Analysen nach
Myers-Briggs-Kriterien [@briggs1985],
liegt die Zahl der Introvertierten bei 33-50%
in der generelle Bevölkerung. [@cain2012quiet]

Wenn man das detaillierte Resultat bei der Kontrollgruppe anschaut (siehe Abbildung
\ref{intro_4}), sieht man immer noch, dass die Introvertierten den grössten Teil
einnehmen und, dass es relativ wenig extravertierte Personen gibt.

\colbegin[2]

![Introversion (Kontrollgruppe) \label{intro_3}](./assets/intro_3.png)

\columnbreak

![Introversion - Detailliert (Kontrollgruppe) \label{intro_4}](./assets/intro_4.png)

\colend

\newpage
### Selbsteinschätzung vs. Test (Kontrollgruppe)
Hier sehen wir den Vergleich zwischen dem Introversionsindex und der Selbsteinschätzung
bei der Kontrollgruppe. Wie in Abbildung \ref{intro_control_idx_v_s} zu sehen,
schätzen sich mehr Personen als extravertiert ein, als der Test danach zeigt.

In Abbildung \ref{control_self_v_idx_it} sieht man, dass sich die Teilnehmer
eher extravertierter einschätzen als sie vom Test eingestuft werden.
Aber im Grossen und Ganzen zeigt die Trendline, einen starken Trend sich als
extravertiert einzustuffen.

\colbegin[2]

![Vergleich zwischen Index und Selbsteinschätzung (Kontrollgruppe) \label{intro_control_idx_v_s}](./assets/intro_control_idx_vs_self.png)

\columnbreak

![Selbsteinschätzung vs. Testresultat (Kontrollgruppe) \label{control_self_v_idx_it}](./assets/control_intro_idx_vs_self_trend.png)

\colend

# Ergebnisse
Die Informatiker haben sich bei der Selbsteinschätzung generell eher als Introvertiert geschätzt. Durch die Auswertung der Daten, gehen wir davon aus, das Informatiker sich nicht richtig einschätzen denn tatsächlich ist das Ergebniss unserer Auswertung, dass Informatiker eher extravertiert sind. Dies gilt aber nur wenn man die Informatiker in nur 2 Kategorien einordnet und zwar der Introversion und der Extraversion. Was heisst dass das Vorurteil "Informatiker sind Introvertiert", in diesem Fall, nicht zutrifft. Dieses Resultat gilt aber nicht mehr, wenn man die Informatiker auch noch als Ambivertiert einteilt. Denn ein grosser Teil der Informatiker ist eher als Ambivertiert einzuteilen, was heisst das, die meisten Informatiker Charakteristiken von Intro- und Extravertierten Teilen und sich nicht nur in eine Kategorie einteilen lassen. 

Wenn wir jetzt die befragten Informatiker mit der Kontrollgruppe vergleichen, kommen wir auf ein überraschendes Resultat. Die Kontrollgruppe hat sich als Introvertierter dargestellt als die Informatiker. Selbst wenn man die Ambivertion mit dazu nimmt, ist die Kontrollgruppe trotzdem introvertierter als die Informatiker.

# Schlussfolgerung
Die Studie hat interessante Informationen, zu den Informatikern freigegeben.
Man kann mit dem Wissen dass Informatiker eher Extravertiert sind,
z.B. den Unterichtsstoff für Informatiker, bei einer höheren Ausbildung
anpassen um die Informationsweitergabe optimal zu gestalten.

# Diskussion
Die Ausgewerteten Daten haben eine Antwort auf das initiale Vorurteil gegeben,
aber wie representativ sind die Daten? 
Die Daten welche für diese Umfrage gesammelt wurden, waren zwar genug um eine
Auswertung und ein Ergebniss zu erhalten, es haben aber nur 30 Informatiker
an der Umfrage teilgenommen. Dies hat zur Folge dass sicht bei einer erneuten Umfrage
mit mehr Teilnehmern, andere Resultate ergeben können, was wiederum die Frage stellt,
wie weit eine neue Studie mit mehr Teilnehmern von diesem Resultat abweichen würde.
Man kann davon ausgehen, dass sich die genauen Zahlen ändern werden und dass das Resultat,
dass Informatiker extravertiert sind nicht mehr zutreffen wird.

Dies zeigen weitere Studien, wie z.B. eine Studie die von der Datingplatform
Parship in zusammenarbeit mit der Universität Bremen durchgeführt worden ist [@cw_parship].
Sie haben die Angaben von 200 Informatikern ausgewertet und sind auf das Gegenteil
von unserem Resultat gekommen.
Ihre Studie hat gezeigt dass Informatiker eher Introvertiert sind.

# Ausblick
Das Vorurteil, ob Informatiker Introvertiert sind, konnten wir beantworten.
Den Vergleich mit der Kontrollgruppe konnten wir zwar durchführen und haben auch
Resultate erhalten, doch da die Resultate der Kontrollgruppe sehr überraschend sind
muss man den McCroskey-Test noch einmal genauer untersuchen und herausfinden ob die
Resultate plausibel sind.

Zusätzlich zu dieser Studie, sollte man die gleiche Studie noch einmal durchführen.
Es sollten jedoch mehr Informatiker befragt werden, um ein repräsentativeres
Resultat bei der Auswertung der Daten zu erhalten.
Zusätzlich kann man die Studie erweitern und die Personen nach mehr Informationen
fragen wie z.B. weitere Infromationen zu Ihrem Aufwachsen um herauszufinden
welche Faktoren im Kindes- und Jugendalter einen Einfluss zur Intraversion
oder Extraversion haben.

# Quellenverzeichnis

::: {#refs}
:::
